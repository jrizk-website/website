package main

type BaseTemplateData struct {
	StylesheetPath string
	HomeURL        string
	ProjectsURL    string
	AboutURL       string
}
