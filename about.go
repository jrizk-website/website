package main

import (
	"net/http"

	"github.com/disgoorg/log"
)

type AboutTemplateData struct {
	BaseTemplateData
}

func serveAbout(w http.ResponseWriter, req *http.Request) {
	data := AboutTemplateData{
		BaseTemplateData: BaseTemplateData{
			StylesheetPath: stylesheetFilepath,
			HomeURL:        homeRoute,
			ProjectsURL:    projectsRoute,
			AboutURL:       aboutRoute,
		},
	}
	err := aboutTemplate.Execute(w, data)
	log.Error(err)
}
