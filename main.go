package main

import (
	"html/template"
	"net/http"

	"github.com/disgoorg/log"
)

// config filepaths
const (
	websiteConfigFilepath string = "/app/webconfig.json"
	localStylesheetDir    string = "/app/css"
	stylesheetDir         string = "css/"
	stylesheetFilepath    string = stylesheetDir + "stylesheet.min.css"
)

var (
	webconfig *WebsiteConfig
)

// template filepaths
const (
	templatesDir             string = "/app/templates"
	baseTemplateFilepath     string = templatesDir + "/base.html"
	homeTemplateFilepath     string = templatesDir + "/home.html"
	aboutTemplateFilepath    string = templatesDir + "/about.html"
	projectsTemplateFilepath string = templatesDir + "/projects.html"
)

// template variables
var (
	homeTemplate     *template.Template
	aboutTemplate    *template.Template
	projectsTemplate *template.Template
)

// routes
const (
	homeRoute     string = "/"
	aboutRoute    string = "/about"
	projectsRoute string = "/projects"
)

func main() {
	var err error
	webconfig, err = NewWebsiteConfig(websiteConfigFilepath)
	if err != nil {
		panic(err)
	}
	log.Debug("parsed website config file")
	log.SetLevel(log.Level(webconfig.LogLevel))

	// set static files css stylesheet
	cssStaticFileDir := http.FileServer(http.Dir(localStylesheetDir))
	// set css file handler
	http.Handle("/css/", http.StripPrefix("/css/", cssStaticFileDir))
	// set page handlers
	http.HandleFunc(homeRoute, serveHome)
	http.HandleFunc(aboutRoute, serveAbout)
	http.HandleFunc(projectsRoute, serveProjects)

	// parse page templates
	homeTemplate = template.Must(template.ParseFiles(baseTemplateFilepath, homeTemplateFilepath))
	aboutTemplate = template.Must(template.ParseFiles(baseTemplateFilepath, aboutTemplateFilepath))
	projectsTemplate = template.Must(template.ParseFiles(baseTemplateFilepath, projectsTemplateFilepath))

	// serve website
	if webconfig.Environment == EnvironmentProd {
		err = http.ListenAndServeTLS(webconfig.Address, webconfig.SSLCertFilepath, webconfig.SSLKeyFilepath, nil)
	} else if webconfig.Environment == EnvironmentDev {
		err = http.ListenAndServe(webconfig.Address, nil)
	} else {
		panic("website environment has not been set")
	}
	log.Fatal(err)
}
