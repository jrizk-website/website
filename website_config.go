package main

import (
	"encoding/json"
	"os"
)

type Environment string

const (
	EnvironmentDev  Environment = "dev"
	EnvironmentProd Environment = "prod"
)

type WebsiteConfig struct {
	Environment     Environment
	SSLCertFilepath string
	SSLKeyFilepath  string
	Address         string
	LogLevel        uint32
}

func NewWebsiteConfig(configFilepath string) (*WebsiteConfig, error) {
	configFile, err := os.Open(configFilepath)
	if err != nil {
		return nil, err
	}
	defer configFile.Close()

	rawConfig := struct {
		Environment     string
		SSLCertFilepath string
		SSLKeyFilepath  string
		Address         string
		LogLevel        string
	}{}
	err = json.NewDecoder(configFile).Decode(&rawConfig)
	if err != nil {
		return nil, err
	}

	var lvl uint32
	switch rawConfig.LogLevel {
	case "panic":
		lvl = 6
	case "fatal":
		lvl = 5
	case "error":
		lvl = 4
	case "warn":
		lvl = 3
	case "info":
		lvl = 2
	case "debug":
		lvl = 1
	case "trace":
		lvl = 0
	default:
		lvl = 1
	}
	return &WebsiteConfig{
		Environment:     Environment(rawConfig.Environment),
		SSLCertFilepath: rawConfig.SSLCertFilepath,
		SSLKeyFilepath:  rawConfig.SSLKeyFilepath,
		Address:         rawConfig.Address,
		LogLevel:        lvl,
	}, nil
}
