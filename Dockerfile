FROM golang:1.19-bullseye
ENV SASS_VERS="1.58.1"
ENV BOOTSTRAP_VERS="5.2.3"

COPY ./color_theme.scss ./

RUN mkdir -p ./bootstrap/scss && \
    mkdir -p /app/templates && \
    mkdir -p /app/css && \
    mkdir -p /app/jonrizk.com && \
    wget "https://github.com/sass/dart-sass/releases/download/${SASS_VERS}/dart-sass-${SASS_VERS}-linux-x64.tar.gz" && \
    tar -xvzf ./dart-sass*.tar.gz && \
    wget "https://github.com/twbs/bootstrap/archive/refs/tags/v${BOOTSTRAP_VERS}.tar.gz" && \
    tar -xvzf ./v${BOOTSTRAP_VERS}.tar.gz && \
    mv ./bootstrap-*/* ./bootstrap/ && \
    ./dart-sass/sass --style compressed ./color_theme.scss /app/css/stylesheet.min.css

WORKDIR /app

COPY ./go.mod ./
COPY ./go.sum ./
RUN go mod download

COPY ./*.go ./
COPY ./templates/*.html ./templates

RUN go build -o /app/website

ENTRYPOINT [ "/app/website" ]
