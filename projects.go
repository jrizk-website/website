package main

import (
	"net/http"

	"github.com/disgoorg/log"
)

type ProjectsTemplateData struct {
	BaseTemplateData
}

func serveProjects(w http.ResponseWriter, req *http.Request) {
	data := ProjectsTemplateData{
		BaseTemplateData: BaseTemplateData{
			StylesheetPath: stylesheetFilepath,
			HomeURL:        homeRoute,
			ProjectsURL:    projectsRoute,
			AboutURL:       aboutRoute,
		},
	}
	err := projectsTemplate.Execute(w, data)
	log.Error(err)
}
