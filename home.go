package main

import (
	"net/http"

	"github.com/disgoorg/log"
)

type HomeTemplateData struct {
	BaseTemplateData
}

func serveHome(w http.ResponseWriter, req *http.Request) {
	data := HomeTemplateData{
		BaseTemplateData: BaseTemplateData{
			StylesheetPath: stylesheetFilepath,
			HomeURL:        homeRoute,
			ProjectsURL:    projectsRoute,
			AboutURL:       aboutRoute,
		},
	}
	err := homeTemplate.Execute(w, data)
	log.Error(err)
}
